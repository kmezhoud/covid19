# Normalization and scaling

- $Case\;(km{^2}) = \frac{Confirmed}{Area}$

- $Density = \frac{Population}{area}= \frac{Persons}{km^{2}}$

- $Spread = \frac{Case\;(km^{2}) \;* \;100}{Density} = \frac{ \frac{Confirmed}{Area}\; *\; 100}{\frac{Persons}{km^{2}}}= \frac{Confirmed \;* \;100 \; *\; km^{2}}{Area \; * \;Persons}$ (rate without unit, 100 is just to make Map lisible)

- $Active = Confirmed \;- \;Recovered\; -\; Death$

- $Mortality = \frac{Death \; * \;100}{Confirmed}$

- $new1Confirmed \;=  last \;Confirmed \;Cases \;(24h)$

- $new3Confirmed \;= \;last \;Confirmed \;Cases \;3 \;days \;ago$

- $g1 = 0.5 * (new1Confirmed+1)/(Confirmed{_}per100k+1)$ 

- $g3 = 0.3 * (new3Confirmed+1)/(Confirmed{_}per100k+1)$

- $g7 = 0.2 * (new7Confirmed+1)/(Confirmed{_}per100k+1)$

- $g = g1 + g3 + g7$

- if $$[g>1 \;or\; g>0.5 \;or\; g>0.2 \;or\; g>0.1 \;or\; g>0.05 \;or\; g>0.02 \;or\; g>0.01 \;or\; g>0.005 \;or\; g>0.002 \;or\; g>0.001 \;or\; g]$$ Then 

$$Containment\; Score\; = \;[0\;or\;1\;or\;2\;or\;3\;or\;4\;or\;5\;or\;6\;or\;7\;or\;8\;or\;9\;or\;10]$$ 

- $CircleMarkers \;radius\; of\; Confirmed\; = \;(Cumulative \;Confirmed)^{1/4}$ 

- $CircleMarkers \;radius \;of \;Deaths \;= \;(Cumulative Death)^{1/4}$ 

- $CircleMarkers \;radius \;of \;Confirmed \;(24h) \;= \;(Confirmed (24h))^{1/4}$ 

- $CircleMarkers \;radius \;of \;Deaths \;(24h) \;= \;(Death (24h))^{1/4}$ 