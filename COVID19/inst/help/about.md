---
title: "Shiny Dashbord for Tracking and modeling COVID-19 datasets"
date: "2020-04-22"
output:
  html_document:
    number_sections: true
    fig_caption: true
    toc: true
    fig_width: 7
    fig_height: 4.5
    theme: cosmo
    highlight: tango
    code_folding: show #hide
    self_contained: no
---



# Shiny Dashbord for Tracking and modeling COVID-19 datasets

* COVOD19 is a Shiny app developed by Karim Mezhoud, Data Scientist at National Center for Nuclear Sciences and Technolgies of Tunisia.

* The goal is to follow the trend of the evolution of this pandemy and estimate when it will be safe to get back to normal life.

* The map overview shows daily confirmed (or positive for US data), recovered , and deaths cases. The two tables quickly show us the classification of dead and confirmed cases in the countries of the world.

* The Spread tab show which Country has the highest Pread rate. Please see below how the Spread is Computed.

* The Prediction Tab implements SIR and SEIRD models mainly used in epidemiology. These two model predict the numbers of  Suceptible, Exposed, Infected, Recovered and Deaths people during the selected range of period. The models are not yet optimized for COVID-19 and the prediction are not True.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3s4FfjMkLWQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


# Data Source

- [COVID19 Countries Times Series](https://github.com/ulklc/covid19-timeseries) 

- [COVID19 USA States data Source](https://covidtracking.com/api/)

- [COVID19 data of  China, Canada, Autralia  Provinces](https://github.com/CSSEGISandData/COVID-19)

- [USA States names, density (p/mi2), population, Area (mi2)](https://worldpopulationreview.com/states/state-densities/)

- [USA States names,  Longitudes, Latitudes, apla2 code ](https://github.com/COVID19Tracking/covid-tracking-dash/blob/master/data/states.csv)

- [US States alpha2 code,  Covid times Series data (starts 2020/03/04 states-daily.csv) ](https://covidtracking.com/api/)

- [Chinese Province Area & Population 2018](http://www.citypopulation.de/en/china/cities/)

- [Macau demography (2020)](https://worldpopulationreview.com/countries/macau-population/)

- [Hong Kong demography (2018)](https://en.wikipedia.org/wiki/Hong_Kong)

- [Inner Mongolia  demography (2020)](https://www.worldometers.info/world-population/mongolia-population/)

- [Australia Province Areas & Population](https://en.wikipedia.org/wiki/States_and_territories_of_Australia)

- [Canada Province Area & Population](https://en.wikipedia.org/wiki/Provinces_and_territories_of_Canada)


# Acknowledgment

* A special Thansk to all developper that are working hard to collect and curate the datasets available in different site.

* A great Thanks for Epidemiologist, Data Scientist who share their works and help developer.
